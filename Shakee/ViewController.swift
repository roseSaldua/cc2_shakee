//
//  ViewController.swift
//  Shakee
//
//  Created by CommudePH0160 on 8/9/19.
//  Copyright © 2019 CommudePH0160. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    let diceArray = ["dice1","dice2","dice3","dice4","dice5", "dice6"]
    
    var randomDiceIndex1: Int = 1
    var randomDiceIndex2: Int = 1

    @IBOutlet weak var diceImage1: UIImageView!
    @IBOutlet weak var diceImage2: UIImageView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateDiceImages()
    }
    
    @IBAction func rollButtonTapped(_ sender: Any) {
        
        updateDiceImages()
    
    }
    
    func updateDiceImages() {
        
        randomDiceIndex1 = Int.random(in: 0 ... 5)
        randomDiceIndex2 = Int.random(in: 0 ... 5)
        
        diceImage1.image = UIImage(named: diceArray[randomDiceIndex1])
        diceImage2.image = UIImage(named: diceArray[randomDiceIndex2])
        
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        updateDiceImages()
    }
    


}

